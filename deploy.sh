#!/bin/sh

# Controlar numero de parametros
if [ "$#" -ne 2 ] ; then
  echo "Usage: $0 entorno aplicacion" >&2
  exit 1
fi

# Controlar el entorno
ENTORNO=
if [ "$1" = "test" ] || [ "$1" = "demo" ] ; then ENTORNO="$1" ; fi
if [ "$ENTORNO" = "" ] ; then
	echo "$1 entorno desconocido" >&2
	exit 1
fi

# Controlar la aplicacion war
WAR=
if [ "$2" = "biid-ridm" ] ; then  WAR="$2" ; fi
if [ "$WAR" = "" ] ; then
	echo "$2 aplicacion desconocida" >&2
	exit 1
fi

WARSRC=$WAR/target/$WAR.war
if [ "$WAR" = "biid-ridm" ] ; then WARSRC="firmamobile-ridm/target/$WAR.war" ; fi

echo "Creando copia de $WAR.war con configuraciones del entorno $ENTORNO"
cp $WARSRC ./deploy
echo ". Copiado"
cd deploy/$ENTORNO/$WAR
find . -type f -exec echo ".. Reemplazar {}" \; -exec jar -uf ../../$WAR.war {} \; 
echo ". Configurado"
cd ../..

SERVER=
if [ "$ENTORNO" = "test" ] ; then  SERVER="biiddevtest.cloudapp.net" ; USER=azureuser ; fi
if [ "$SERVER" = "" ] ; then
	echo "Servidor de despliegue no definido para el entorno $ENTORNO" >&2
	exit 1
fi

# FTP
echo "Envio de $WAR.war"
sftp $USER@$SERVER << EOF
cd deploy
put $WAR.war
chmod 644 $WAR.war
ls -l
quit
EOF

# Undeploy y deploy

echo "Undeploy de $WAR"
wget -O $WAR-undeploy.txt -o $WAR-undeploy.log --user admin --ask-password --no-check-certificate https://$SERVER/manager/undeploy?path=/$WAR
cat $WAR-undeploy.txt

echo "Deploy de $WAR"
wget -O $WAR-deploy.txt -o $WAR-deploy.log --user admin --ask-password --no-check-certificate https://$SERVER/manager/deploy?path=/$WAR\&war=/home/azureuser/deploy/$WAR.war
cat $WAR-deploy.txt
